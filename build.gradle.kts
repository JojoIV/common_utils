plugins {
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.kotlin.multiplatform) apply false
    alias(libs.plugins.compose.compiler) apply false
    alias(libs.plugins.kotlinx.serialization) apply false
//    alias(libs.plugins.dokka) apply false
    alias(libs.plugins.maven.publish) apply false
    alias(libs.plugins.kotlin.android) apply false
}

allprojects {
    group = "net.novagamestudios.common_utils"

    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}

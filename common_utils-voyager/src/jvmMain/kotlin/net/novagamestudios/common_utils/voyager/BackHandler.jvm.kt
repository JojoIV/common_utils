package net.novagamestudios.common_utils.voyager

import androidx.compose.runtime.Composable

@Composable
internal actual fun DefaultBackHandler(onBack: () -> Unit) { }
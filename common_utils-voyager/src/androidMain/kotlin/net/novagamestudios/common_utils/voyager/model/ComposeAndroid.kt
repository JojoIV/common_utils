package net.novagamestudios.common_utils.voyager.model

import android.app.Application
import androidx.compose.runtime.Composable
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.screen.Screen
import net.novagamestudios.common_utils.compose.application


@Composable
inline operator fun <reified T : ScreenModel, reified A : Application> GlobalScreenModelFactory<A, A, T>.getValue(
    thisRef: Any?,
    property: Any?
): T {
    val app = application<A>()
    return rememberGlobalScreenModel(repositoryProvider = app, app = app, factory = this)
}

context(S)
@Composable
inline operator fun <S : Screen, reified T : ScreenModel, reified A : Application> ScreenModelFactory<A, S, T>.getValue(
    thisRef: Any?,
    property: Any?
): T = rememberScreenModel(repositoryProvider = application<A>(), factory = this)

package net.novagamestudios.common_utils.voyager.scaffold

import androidx.compose.runtime.Composable
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import java.io.Serializable


interface ScaffoldContent : Serializable

interface ContentWithBackNavigation : ScaffoldContent {
    @Composable
    fun backNavigationHandler(navigator: Navigator): BackNavigationHandler = navigator.currentScaffoldContent()?.backNavigationHandler(navigator) ?: BackNavigationHandler.Disabled
}

context (C)
@Composable
inline fun <reified C> Navigator.currentScaffoldContent(): C? {
    val screen = lastItem
    if (screen !is ScaffoldContentProvider) return null
    val scaffoldContent = screen.scaffoldContent
    if (scaffoldContent == this@C) return null
    return screen.scaffoldContent as? C
}

interface ScaffoldContentProvider {
    val scaffoldContent: ScaffoldContent
}

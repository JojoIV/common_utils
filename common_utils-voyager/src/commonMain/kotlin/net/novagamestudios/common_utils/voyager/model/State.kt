package net.novagamestudios.common_utils.voyager.model

import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import net.novagamestudios.common_utils.compose.state.collectAsStateIn


context (CoroutineScope)
fun <T> Flow<T>.collectAsStateHere(initialValue: T) = collectAsStateIn(this@CoroutineScope, initialValue)

context (CoroutineScope)
fun <T> StateFlow<T>.collectAsStateHere() = collectAsStateIn(this@CoroutineScope)

context (ScreenModel)
fun <T> Flow<T>.collectAsStateHere(initialValue: T) = collectAsStateIn(screenModelScope, initialValue)

context (ScreenModel)
fun <T> StateFlow<T>.collectAsStateHere() = collectAsStateIn(screenModelScope)

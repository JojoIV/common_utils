package net.novagamestudios.common_utils.voyager

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow


interface BackNavigationHandler {

    /**
     * @return true if back navigation ui components should be shown.
     * false does not mean that the navigation can not be handled
     */
    fun canNavigateBack(): Boolean

    /**
     * @return true if the navigation was handled and false if it should be passed on
     */
    fun onNavigateBack(): Boolean

    @Composable
    fun BackHandler() = DefaultBackHandler {
        onNavigateBack()
    }

    companion object {
        val Disabled = object : BackNavigationHandler {
            override fun canNavigateBack() = false
            override fun onNavigateBack() = true
            @Composable
            override fun BackHandler() { }
        }
        @Suppress("MemberVisibilityCanBePrivate")
        fun forNavigator(navigator: Navigator) = object : BackNavigationHandler {
            override fun canNavigateBack() = navigator.canPop
            override fun onNavigateBack(): Boolean {
                navigator.pop()
                return true
            }
        }
        @Composable
        fun default(): BackNavigationHandler {
            val navigator = LocalNavigator.currentOrThrow
            return remember(navigator) { forNavigator(navigator) }
        }
        infix fun BackNavigationHandler?.then(parent: BackNavigationHandler?) = object :
            BackNavigationHandler {
            override fun canNavigateBack(): Boolean {
                return this@then?.canNavigateBack() == true || parent?.canNavigateBack() == true
            }
            override fun onNavigateBack(): Boolean {
                return this@then?.onNavigateBack() == true || parent?.onNavigateBack() == true
            }
        }

        fun derivedOf(
            canNavigateBack: () -> Boolean,
            onNavigateBack: () -> Boolean
        ) = object : BackNavigationHandler {
            private val _canNavigateBack by derivedStateOf(canNavigateBack)
            override fun canNavigateBack() = _canNavigateBack
            override fun onNavigateBack() = onNavigateBack()
        }
    }
}

@Composable
internal expect fun DefaultBackHandler(onBack: () -> Unit)

@Composable
fun AnimatedBackNavigationIcon(
    handler: BackNavigationHandler,
    enter: EnterTransition = expandHorizontally(),
    exit: ExitTransition = shrinkHorizontally(),
) = AnimatedVisibility(
    visible = handler.canNavigateBack(),
    enter = enter,
    exit = exit
) {
    IconButton(onClick = { handler.onNavigateBack() }) {
        Icon(Icons.AutoMirrored.Filled.ArrowBack, "Back")
    }
}

package net.novagamestudios.common_utils.voyager

import androidx.compose.runtime.Composable
import androidx.compose.runtime.NonSkippableComposable
import cafe.adriel.voyager.core.annotation.InternalVoyagerApi
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import net.novagamestudios.common_utils.compose.logging.debug
import net.novagamestudios.common_utils.core.logging.debug


@OptIn(InternalVoyagerApi::class)
@NonSkippableComposable
@Composable
fun debugNavigation() {
    fun StringBuilder.addNavigator(navigator: Navigator) {
        navigator.parent?.let { addNavigator(it) }
        val indent = "  ".repeat(navigator.level)
        appendLine("${indent}Navigator: ${navigator.key}")
        navigator.items.forEach { screen ->
            appendLine("${indent}- ${screen.key}: ${screen::class.qualifiedName}")
        }
    }
    val navigator = LocalNavigator.currentOrThrow
    val result = StringBuilder().apply {
        addNavigator(navigator)
    }.toString()
    debug { result }
}

tailrec fun Navigator.findNavigatorOrNull(predicate: (Navigator) -> Boolean): Navigator? {
    if (predicate(this)) return this
    return parent?.findNavigatorOrNull(predicate)
}

@OptIn(InternalVoyagerApi::class)
fun Navigator.requireWithKey(key: String) = findNavigatorOrNull { it.key == key }
    ?: error("Navigator with key '$key' not found")


@Composable
inline fun <reified T : Screen> nearestScreenOrNull(): T? {
    var navigator = LocalNavigator.current
    while (navigator != null) {
        val screen = navigator.items.filterIsInstance<T>().lastOrNull()
        if (screen != null) return screen
        navigator = navigator.parent
    }
    return null
}

@Composable
inline fun <reified T : Screen> nearestScreen(): T {
    return nearestScreenOrNull() ?: error("No screen of type ${T::class.qualifiedName} found")
}

@Suppress("RecursivePropertyAccessor")
val Navigator.root: Navigator get() = parent?.root ?: this

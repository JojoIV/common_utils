package net.novagamestudios.common_utils.voyager.model

import androidx.compose.runtime.Composable
import cafe.adriel.voyager.core.model.ScreenModel

interface ScreenModelProvider<out T : ScreenModel> {
    @get:Composable
    val model: T
}

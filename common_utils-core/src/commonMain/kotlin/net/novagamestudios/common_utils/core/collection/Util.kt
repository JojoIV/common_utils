package net.novagamestudios.common_utils.core.collection

import kotlin.collections.ArrayDeque

//inline fun <T, S : Comparable<S>> Sequence<T>.nLowestBy(n: Int, scoringFunction: (T) -> S): Collection<Pair<S, T>> {
//    val lowestCandidates: SortedSet<Pair<S, T>> = TreeSet(
//        Comparator.comparing<Pair<S, T>, S> { p -> p.first }
//            .thenComparing { p -> p.second.hashCode() }
//    )
//
//    val candidateIterator = iterator()
//    repeat(n) {
//        if (!candidateIterator.hasNext()) return@repeat
//        val candidate = candidateIterator.next()
//        val score = scoringFunction(candidate)
//        lowestCandidates += score to candidate
//    }
//
//    if (candidateIterator.hasNext()) {
//        var max = lowestCandidates.last().first
//        do {
//            val candidate = candidateIterator.next()
//            val score = scoringFunction(candidate)
//            if (score < max) {
//                for (p in lowestCandidates) {
//                    if (p.first != max) continue
//                    lowestCandidates.remove(p)
//                    break
//                }
//                lowestCandidates += score to candidate
//                max = lowestCandidates.last().first
//            }
//        } while (candidateIterator.hasNext())
//    }
//
//    return lowestCandidates
//}


//inline fun <T, S : Comparable<S>> Sequence<T>.nHighestBy(n: Int, scoringFunction: (T) -> S): SortedSet<Pair<S, T>> {
//    if (n < 0) throw IllegalArgumentException("n < 0")
//    if (n == 0) return sortedSetOf()
//
//    val highestCandidates: SortedSet<Pair<S, T>> = TreeSet(
//        Comparator.comparing<Pair<S, T>, S> { p -> p.first }
//            .thenComparing { p -> p.second.hashCode() }
//            .reversed()
//    )
//
//    val candidateIterator = iterator()
//    repeat(n) {
//        if (!candidateIterator.hasNext()) return@repeat
//        val candidate = candidateIterator.next()
//        val score = scoringFunction(candidate)
//        highestCandidates += score to candidate
//    }
//
//    if (candidateIterator.hasNext()) {
//        var min = highestCandidates.last().first
//        do {
//            val candidate = candidateIterator.next()
//            val score = scoringFunction(candidate)
//            if (score > min) {
//                for (p in highestCandidates) {
//                    if (p.first != min) continue
//                    highestCandidates.remove(p)
//                    break
//                }
//                highestCandidates += score to candidate
//                min = highestCandidates.last().first
//            }
//        } while (candidateIterator.hasNext())
//    }
//
//    return highestCandidates
//}

inline fun <T> Collection<T>.averageOf(selector: (T) -> Double) = sumOf(selector) / size
fun Collection<Double>.average() = averageOf { it }


fun <T> Sequence<T>.preload(n: Int): Sequence<T> {
    val iterator = iterator()
    val queue = ArrayDeque<T>(n)
    while (queue.size < n && iterator.hasNext()) {
        queue.addLast(iterator.next())
    }
    return sequence {
        while (queue.isNotEmpty()) {
            yield(queue.removeFirst())
            if (iterator.hasNext()) queue.addLast(iterator.next())
        }
    }
}



fun <T : Comparable<T>, R : Comparable<R>> ClosedRange<T>.map(transform: (T) -> R): ClosedRange<R> {
    return transform(start)..transform(endInclusive)
}


fun ClosedRange<Int>.toIntRange() = IntRange(start, endInclusive)



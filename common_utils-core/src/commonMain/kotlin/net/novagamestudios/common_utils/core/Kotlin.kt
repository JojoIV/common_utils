package net.novagamestudios.common_utils.core


context (T)
fun <T> context(): T = this@T

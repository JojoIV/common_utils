package net.novagamestudios.common_utils.core

import java.util.Locale

infix fun Any?.format(format: String) = format(format, Locale.ENGLISH)
fun Any?.format(format: String, locale: Locale? = Locale.ENGLISH) = String.format(locale, format, this)
infix fun String.charFlipDistance(other: String): Double = (0..<kotlin.math.max(length, other.length)).sumOf {
    if (getOrNull(it) != other.getOrNull(it)) 1.0
    else 0.0
}
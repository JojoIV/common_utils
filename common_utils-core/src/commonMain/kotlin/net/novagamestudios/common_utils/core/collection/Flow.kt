package net.novagamestudios.common_utils.core.collection

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.DEFAULT_CONCURRENCY
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.produceIn
import kotlinx.coroutines.flow.reduce
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.verbose
import java.util.LinkedList
import kotlin.coroutines.CoroutineContext


suspend inline fun <T, S : Comparable<S>> Flow<T>.nMaxBy(n: Int, crossinline scoringFunction: suspend (T) -> S): List<T> {
    if (n < 0) throw IllegalArgumentException("n < 0")
    if (n == 0) return emptyList()
    if (n == 1) return listOf(maxBy(scoringFunction))

    val highest = LinkedList<T>()

    collect { item ->
        val score = scoringFunction(item)
        if (highest.isEmpty() || scoringFunction(highest.last) > score) {
            if (highest.size < n) highest.add(item)
            return@collect
        }
        val iterator = highest.listIterator()
        while (iterator.hasNext()) {
            val toCompare = iterator.next()
            if (score > scoringFunction(toCompare)) {
                iterator.previous()
                iterator.add(item)
                if (highest.size > n) highest.removeLast()
                return@collect
            }
        }
        if (highest.size < n) iterator.add(item)
    }

    return highest
}

@OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
fun <T, R> Flow<T>.concurrentTransformNoOrder(
    context: CoroutineContext = Dispatchers.Default,
    concurrency: Int = DEFAULT_CONCURRENCY,
    transform: suspend (T) -> R
): Flow<R> = this
    .flatMapMerge(concurrency) { flow { emit(transform(it)) } }
    .flowOn(context)

context (CoroutineScope)
@OptIn(FlowPreview::class)
fun <T, R> Flow<T>.concurrentTransformOrder(
    context: CoroutineContext = Dispatchers.Default,
    concurrency: Int = DEFAULT_CONCURRENCY,
    transform: suspend (T) -> R
): Flow<R> = this
    .map { async(context) { transform(it) } }
    .buffer(concurrency)
    .map { it.await() }

suspend inline fun <T, R : Comparable<R>> Flow<T>.maxBy(crossinline selector: suspend (T) -> R): T = this
    .map { it to selector(it) }
    .reduce { (v1, c1), (v2, c2) ->
        if (c2 > c1) v2 to c2 else v1 to c1
    }
    .first

inline fun <T> Flow<T>.onEachIndexed(crossinline action: suspend (Int, T) -> Unit): Flow<T> = flow {
    var index = 0
    collect { value ->
        action(index++, value)
        emit(value)
    }
}

fun <T> Flow<T>.diffs(): Flow<Pair<T, T>> = flow {
    var hasPrevious = false
    var previous: T? = null
    collect { value ->
        @Suppress("UNCHECKED_CAST")
        if (hasPrevious) emit(previous as T to value)
        previous = value
        hasPrevious = true
    }
}



@OptIn(ExperimentalCoroutinesApi::class)
fun <T> Flow<T>.delayedLatest(delayMillis: Long) = flow {
    coroutineScope {
        val channel = produce { collect { value -> send(value) } }
        for (value in channel) {
            var latest = value
            val updater = launch {
                for (newer in channel) latest = newer
            }

            try {
                delay(delayMillis)
                updater.cancelAndJoin()
            } catch (e: CancellationException) {
                emit(latest)
                throw e
            }
            emit(latest)
        }
    }
}

private val logger = Logger("TEST")

fun <T> Flow<Flow<T>>.delayedLatestOrLastFlatten(delayMillis: Long): Flow<T> = channelFlow {
    coroutineScope {
        val flowsChannel = produceIn(this)
        var collector: Job? = null
        for (someFlow in flowsChannel) {
            logger.verbose { "new somFlow" }
            collector?.cancelAndJoin()
            logger.verbose { "canceled previous collector" }
            collector = launch {
                val values = someFlow.produceIn(this)
                for (value in values) {
                    var latest = value
                    val updater = launch {
                        for (newer in values) latest = newer
                    }
                    try {
                        delay(delayMillis)
                        updater.cancelAndJoin()
                    } finally {
                        send(latest)
                    }
                }
            }
        }
    }
}


fun <T, R> StateFlow<T>.mapState(
    transform: (T) -> R
): StateFlow<R> = object : StateFlow<R> {
    override val replayCache: List<R> get() = this@mapState.replayCache.map(transform)
    override val value: R get() = transform(this@mapState.value)
    override suspend fun collect(collector: FlowCollector<R>): Nothing {
        this@mapState.collect { value -> collector.emit(transform(value)) }
    }
}





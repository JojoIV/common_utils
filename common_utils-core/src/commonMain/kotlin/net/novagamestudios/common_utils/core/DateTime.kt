package net.novagamestudios.common_utils.core

import kotlinx.datetime.LocalTime
import kotlin.time.Duration
import kotlin.time.Duration.Companion.nanoseconds


operator fun LocalTime.minus(other: LocalTime): Duration {
    return (toNanosecondOfDay() - other.toNanosecondOfDay()).nanoseconds
}

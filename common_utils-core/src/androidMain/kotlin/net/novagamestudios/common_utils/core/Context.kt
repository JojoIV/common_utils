package net.novagamestudios.common_utils.core

import android.content.Context
import android.widget.Toast


fun Context.toastShort(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun Context.toastLong(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

package net.novagamestudios.common_utils.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.platform.LocalInspectionMode

val isInPreview @Composable get() = rememberUpdatedState(LocalInspectionMode.current)

@Composable
inline fun <T> T.inPreview(lazy: () -> T) = if (isInPreview.value) lazy() else this
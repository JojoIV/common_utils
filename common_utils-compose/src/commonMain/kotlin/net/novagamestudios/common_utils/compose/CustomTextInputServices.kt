@file:Suppress("DEPRECATION")

package net.novagamestudios.common_utils.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.text.input.BackspaceCommand
import androidx.compose.ui.text.input.CommitTextCommand
import androidx.compose.ui.text.input.EditCommand
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.ImeOptions
import androidx.compose.ui.text.input.PlatformTextInputService
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.TextInputService
import androidx.compose.ui.text.input.TextInputSession

abstract class SimpleTextInputService : PlatformTextInputService {

    @Suppress("MemberVisibilityCanBePrivate")
    protected var imeOptions: ImeOptions = ImeOptions()
        private set
    protected var onEditCommand: (List<EditCommand>) -> Unit = { }
        private set
    protected var onImeActionPerformed: (ImeAction) -> Unit = { }
        private set

    protected open fun onText(text: String) {
        onEditCommand(listOf(CommitTextCommand(text, 1)))
    }
    protected open fun onBackspace() {
        onEditCommand(listOf(BackspaceCommand()))
    }
    protected open fun onSubmit() {
        onImeActionPerformed(imeOptions.imeAction)
    }

    override fun startInput(
        value: TextFieldValue,
        imeOptions: ImeOptions,
        onEditCommand: (List<EditCommand>) -> Unit,
        onImeActionPerformed: (ImeAction) -> Unit
    ) {
        this.imeOptions = imeOptions
        this.onEditCommand = onEditCommand
        this.onImeActionPerformed = onImeActionPerformed
        showSoftwareKeyboard()
    }
    override fun stopInput() {
        hideSoftwareKeyboard()
        this.imeOptions = ImeOptions()
        this.onEditCommand = { }
        this.onImeActionPerformed = { }
    }
    override fun updateState(oldValue: TextFieldValue?, newValue: TextFieldValue) { }
}

@Composable
fun SwapTextInputService(
    textInputService: TextInputService?,
    content: @Composable () -> Unit
) {
    val swappableTextInputService = remember { SwappableTextInputService() }
    swappableTextInputService.current = textInputService ?: LocalTextInputService.current
    CompositionLocalProvider(
        LocalTextInputService provides remember { TextInputService(swappableTextInputService) },
        content = content
    )
}

class SwappableTextInputService(
    initial: TextInputService? = null
) : PlatformTextInputService {
    var current = initial
        set(value) {
            if (current != value && currentSession != null) {
                stopInput()
                field = value
                startInput(textFieldValue, imeOptions, onEditCommand, onImeActionPerformed)
            } else {
                field = value
            }
        }

    private lateinit var textFieldValue: TextFieldValue
    private lateinit var imeOptions: ImeOptions
    private lateinit var onEditCommand: (List<EditCommand>) -> Unit
    private lateinit var onImeActionPerformed: (ImeAction) -> Unit
    private var currentSession: TextInputSession? = null

    override fun hideSoftwareKeyboard() {
        currentSession?.hideSoftwareKeyboard()
    }

    override fun showSoftwareKeyboard() {
        currentSession?.showSoftwareKeyboard()
    }

    override fun startInput(
        value: TextFieldValue,
        imeOptions: ImeOptions,
        onEditCommand: (List<EditCommand>) -> Unit,
        onImeActionPerformed: (ImeAction) -> Unit
    ) {
        this.textFieldValue = value
        this.imeOptions = imeOptions
        this.onEditCommand = onEditCommand
        this.onImeActionPerformed = onImeActionPerformed
        currentSession = current?.startInput(value, imeOptions, onEditCommand, onImeActionPerformed)
    }

    override fun stopInput() {
        currentSession?.let { current?.stopInput(it) }
        currentSession = null
    }

    override fun updateState(oldValue: TextFieldValue?, newValue: TextFieldValue) {
        currentSession?.updateState(oldValue, newValue)
    }

    override fun notifyFocusedRect(rect: Rect) {
        currentSession?.notifyFocusedRect(rect)
    }
}



package net.novagamestudios.common_utils.compose

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.LayoutDirection
import kotlin.math.roundToInt

data class DashedShape(
    val horizontal: Dp,
    val vertical: Dp
) : Shape {
    constructor(size: Dp) : this(size, size)
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ) = Outline.Generic(Path().apply {
        val stepsCount = with(density) {
            IntSize(
                (size.width / horizontal.toPx()).roundToInt() + 1,
                (size.height / vertical.toPx()).roundToInt() + 1
            )
        }
        val dotSize = Size(
            width = size.width / stepsCount.width,
            height = size.height / stepsCount.height
        )
        for (x in 0 until stepsCount.width step 2) for (y in 0 until stepsCount.height step 2) {
            addRect(
                Rect(
                    offset = Offset(x * dotSize.width, y * dotSize.height),
                    size = dotSize
                )
            )
        }
        close()
    })
}

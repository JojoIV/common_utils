package net.novagamestudios.common_utils.compose

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.AnimationState
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateTo
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Indication
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.gestures.awaitEachGesture
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.detectTransformGestures
import androidx.compose.foundation.gestures.waitForUpOrCancellation
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.TabPosition
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollDispatcher
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ScaleFactor
import androidx.compose.ui.layout.layout
import androidx.compose.ui.layout.onPlaced
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.isUnspecified
import androidx.compose.ui.unit.lerp
import androidx.compose.ui.unit.round
import androidx.compose.ui.unit.toOffset
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.milliseconds

fun Modifier.backgroundGlow(
    color: Color
) = shadow(
    elevation = 10.dp,
    shape = CircleShape,
    clip = false,
    spotColor = color
)

fun Modifier.animateWidth(
    animationSpec: AnimationSpec<Dp> = spring(),
) = composed {
    var target by remember { mutableStateOf(Dp.Unspecified) }
    val current = remember { AnimationState(Dp.VectorConverter, target) }
    key(Unit) {
        LaunchedEffect(target) {
            current.animateTo(target, animationSpec)
        }
    }
    layout { measurable, constraints ->
        val placeable = measurable.measure(constraints)
        target = placeable.width.toDp()
        layout(
            width = current.value.takeUnless { it.isUnspecified }?.roundToPx() ?: placeable.width,
            height = placeable.height
        ) {
            placeable.placeRelative(IntOffset.Zero)
        }
    }
}

fun Modifier.animateHeight(
    animationSpec: AnimationSpec<Dp> = spring(),
) = composed {
    var target by remember { mutableStateOf(Dp.Unspecified) }
    val current = remember { AnimationState(Dp.VectorConverter, target) }
    key(Unit) {
        LaunchedEffect(target) {
            current.animateTo(target, animationSpec)
        }
    }
    layout { measurable, constraints ->
        val placeable = measurable.measure(constraints)
        target = placeable.height.toDp()
        layout(
            width = placeable.width,
            height = current.value.takeUnless { it.isUnspecified }?.roundToPx() ?: placeable.height
        ) {
            placeable.placeRelative(IntOffset.Zero)
        }
    }
}

fun Modifier.animateSize(
    animationSpec: AnimationSpec<DpSize> = spring(),
) = composed {
    var target by remember { mutableStateOf(DpSize.Unspecified) }
    val current = remember { AnimationState(DpSize.VectorConverter, target) }
    key(Unit) {
        LaunchedEffect(target) {
            current.animateTo(target, animationSpec)
        }
    }
    layout { measurable, constraints ->
        val placeable = measurable.measure(constraints)
        target = DpSize(placeable.width.toDp(), placeable.height.toDp())
        val size = current.value.takeUnless { it.isUnspecified }
        layout(
            width = size?.width?.roundToPx() ?: placeable.width,
            height = size?.height?.roundToPx() ?: placeable.height
        ) {
            placeable.placeRelative(IntOffset.Zero)
        }
    }
}

fun Modifier.fakeSize(center: Boolean = false, faker: Density.(IntSize) -> IntSize) = layout { measurable, constraints ->
    val placeable = measurable.measure(constraints)
    val size = faker(IntSize(placeable.width, placeable.height))
    layout(size.width, size.height) {
        placeable.placeRelative(
            if (center) IntOffset(
                (size.width - placeable.width) / 2,
                (size.height - placeable.height) / 2
            ) else IntOffset.Zero
        )
    }
}

fun Modifier.realScale(center: Boolean = false, scale: Density.() -> ScaleFactor) = this
    .fakeSize(center = center) { (it * scale()).round() }
    .graphicsLayer { this.scale = scale() }

fun Modifier.alpha(getter: () -> Float) = graphicsLayer {
    alpha = getter()
}

fun Modifier.slideInFromTop(factorGetter: () -> Float) = layout { measurable, constraints ->
    measurable.measure(constraints).let {
        val factor = factorGetter()
        layout(
            it.width,
            (it.height * factor).roundToInt()
        ) {
            it.placeRelative(
                IntOffset(
                    0,
                    -(it.height * (1f - factor)).roundToInt()
                )
            )
        }
    }
}

fun Modifier.clipWithOverdraw(
    overdraw: PaddingValues
) = this
    .layout { measurable, constraints ->
        measurable
            .measure(constraints)
            .let {
                val (start, top, end, bottom) = overdraw.calculatePadding(layoutDirection)
                layout(
                    it.width - (start + end).roundToPx(),
                    it.height - (top + bottom).roundToPx()
                ) {
                    it.placeRelative(IntOffset(-start.roundToPx(), -top.roundToPx()))
                }
            }
    }
    .clipToBounds()
    .padding(overdraw)


fun Modifier.simpleClickable(
    delayPressInteraction: Boolean = false,
    onClick: () -> Unit
) = composed {
    simpleClickable(
        interactionSource = remember { MutableInteractionSource() },
        indication = LocalIndication.current,
        delayPressInteraction = delayPressInteraction,
        onClick = onClick
    )
}
fun Modifier.simpleClickable(
    interactionSource: MutableInteractionSource,
    indication: Indication?,
    delayPressInteraction: Boolean = false,
    onClick: () -> Unit
) = this
    .indication(interactionSource, indication)
    .pointerInput(Unit) {
        coroutineScope {
            awaitEachGesture {
                val down = awaitFirstDown()

                val press = PressInteraction.Press(down.position)
                val delayJob = launch {
                    if (delayPressInteraction) {
                        delay(100.milliseconds)
                    }
                    interactionSource.emit(press)
                }

                val up = waitForUpOrCancellation()

                launch {
                    if (delayJob.isActive) {
                        delayJob.cancelAndJoin()
                        if (up != null) {
                            val press2 = PressInteraction.Press(down.position)
                            val release = PressInteraction.Release(press2)
                            interactionSource.emit(press2)
                            interactionSource.emit(release)
                        }
                    } else {
                        interactionSource.emit(
                            if (up != null) PressInteraction.Release(press)
                            else PressInteraction.Cancel(press)
                        )
                    }
                }

                if (up != null) {
                    up.consume()
                    onClick()
                }
            }
        }
    }

fun Modifier.zoomable() = composed {
    var panning by remember { mutableStateOf(Offset.Zero) }
    var scale by remember { mutableFloatStateOf(0f) }
    var containerSize by remember { mutableStateOf(IntSize.Zero) }
    return@composed this
        .layout { measurable, constraints ->
            val placeable = measurable.measure(
                constraints.copy(
                    maxWidth = Constraints.Infinity,
                    maxHeight = Constraints.Infinity
                )
            )
            val size = IntSize(
                placeable.width.coerceAtMost(constraints.maxWidth),
                placeable.height.coerceAtMost(constraints.maxHeight)
            )
            containerSize = size
            layout(size.width, size.height) {
                placeable.placeRelativeWithLayer(
                    (panning - Offset(
                        (placeable.width - size.width) / 2f,
                        (placeable.height - size.height) / 2f
                    )).round()
                )
            }
        }
        .pointerInput(Unit) {
            detectTransformGestures { centroid, pan, zoom, _ ->
                val scaleBefore = scale
                scale *= zoom
                scale = scale.coerceIn(0.7f, 3f)

                val delta = pan + ((centroid - size.center.toOffset()) * (1f - scale / scaleBefore))
                panning += delta
                val maxPanning = (size.center - containerSize.center).toOffset()
                panning = Offset(
                    panning.x.coerceIn(-maxPanning.x, maxPanning.x),
                    panning.y.coerceIn(-maxPanning.y, maxPanning.y)
                )
            }
        }
        .run {
            with(LocalDensity.current) {
                padding(containerSize.width.toDp() / 2f, containerSize.height.toDp() / 2f)
            }
        }
        .realScale(center = true) { ScaleFactor(scale, scale) }
        .run {
            onPlaced {
                // If not scaled, scale the content to fit container
                if (scale > 0f) return@onPlaced
//                val hScale =
//                    containerSize.width / (FixedCellSize * (expectedPuzzleColumnCount + 1)).toPx()
//                val vScale =
//                    containerSize.height / (FixedCellSize * (expectedPuzzleRowCount + 1)).toPx()
                val hScale = containerSize.width / it.size.width.toFloat()
                val vScale = containerSize.height / it.size.height.toFloat()
                scale = min(hScale, vScale)
            }
        }
}



fun Modifier.tabIndicatorOffset(
    pagerState: PagerState,
    tabPositions: List<TabPosition>
): Modifier = composed {
    val offsetFraction = pagerState.currentPageOffsetFraction
    val offsetFractionFromLeft = offsetFraction + if (offsetFraction < 0) 1 else 0

    val leftTabIndex = pagerState.currentPage + if (offsetFraction < 0) -1 else 0
    val rightTabIndex = pagerState.currentPage + if (offsetFraction < 0) 0 else 1
    val leftTab = tabPositions[leftTabIndex.coerceIn(tabPositions.indices)]
    val rightTab = tabPositions[rightTabIndex.coerceIn(tabPositions.indices)]

    val width = lerp(
        leftTab.width,
        rightTab.width,
        offsetFractionFromLeft
    )
    this
        .fillMaxWidth()
        .wrapContentSize(Alignment.BottomStart)
        .offset {
            val offset = lerp(
                leftTab.left,
                rightTab.left,
                offsetFractionFromLeft
            )
            IntOffset(offset.roundToPx(), 0)
        }
        .width(width)
}



fun Modifier.maskedCircleIcon(
    color: Color
) = this
    .graphicsLayer { alpha = 0.99f }
    .drawWithContent {
        drawContent()
        drawCircle(
            color = color,
            blendMode = BlendMode.SrcOut
        )
    }
    .defaultMinSize(24.dp)


inline fun Modifier.thenIf(condition: Boolean, block: Modifier.() -> Modifier) = if (condition) block() else this


fun Modifier.nestedScroll(
    connection: NestedScrollConnection?,
    dispatcher: NestedScrollDispatcher? = null
): Modifier {
    return if (connection != null) nestedScroll(connection, dispatcher) else this
}

